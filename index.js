const multer =          require('multer');
const express =         require('express');
const { QuickDB } =     require("quick.db");
const bodyParser =      require('body-parser');
const exifr =           require('exifr');
const fs =              require('fs');
const https =           require('https');
const session =         require('express-session');
const passport =        require('passport');
const LocalStrategy =   require('passport-local').Strategy;

require('dotenv').config();

// GLOBALS
var current_moderation = [];

// =========== Express Conf =========== //
const e_app = express();
e_app.set('view engine', 'ejs');
e_app.set('trust proxy', true);
//e_app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));

// =========== DB Conf =========== //
const db = new QuickDB();
const jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false })
e_app.use(urlencodedParser);


// =========== Multer Conf =========== //
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'fileserve')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9) // generate random file name
        cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop())
    }
});
const upload = multer({ storage: storage });

// =========== Passport Conf =========== //
e_app.use(session({
    secret: 'mysecretkey',
    resave: true,
    saveUninitialized: true
}));
e_app.use(passport.initialize());
e_app.use(passport.session());

passport.use(new LocalStrategy(
    async function(username, password, done) {
      if (username == process.env.MODUSER && password == process.env.MODPASS){
        return done(null, "admin");
      }
      else {
        return done(null, false, { message: 'Incorrect username or password.' });
      }
    }
));

passport.serializeUser(function(username, done) {
    done(null, username);
});

passport.deserializeUser(async function(username, done) {
    done(null, username);
});

// =========== Router Conf =========== //
e_app.get('/', function(_req, res) { // main page
    res.render("index",{
    })
});
e_app.get('/map', function(_req, res) { // map page
    res.render("map",{
    })
});
e_app.get('/tab', function(_req, res) { // table page
    res.render("tab",{
    })
});
e_app.get('/test', function(_req, res) { // table page
    res.render("test",{
    })
});
e_app.get('/moderate', function(req, res) { // table page
    if (req.isAuthenticated()) {
        res.render("moderate");
    } else {
        res.render("login");
    }
});
e_app.post('/login', function(req, res, next) {
    console.info('REQUESTED LOGIN');
    next();
    }, passport.authenticate('local', {
        successRedirect: '/moderate',
        failureRedirect: '/moderate'
}));
e_app.get('/logout', (req, res) => {
    req.logout((err) => {
        if (err) {
        console.warn(err);
        }
        res.redirect('/');
    });
});


// =========== API Conf =========== //
e_app.post('/api/upload', upload.single('image'), async function(req, res){
    console.log("Attempting upload...");
    let exifdata = await exifr.parse(req.file.path);
    
    
    let filename = req.file.filename.replace(/\.[^/.]+$/, "") // filename without extension (FOR ID)
    var camera = "";
    var timestamp = "";
    var timetype = "";

    if (exifdata.Make && exifdata.Model){ // If camera model exists in exif, use it.
        camera = exifdata.Make + ", " + exifdata.Model
    }

    if (exifdata.CreateDate){ // best case date info
        timestamp = exifdata.CreateDate;
        timetype = "Capture date";
    }
    else if (exifdata.ModifyDate){ // second best date info
        timestamp = exifdata.ModifyDate;
        timetype = "Last modified date";
    }else{                      // worst case date info
        timestamp = Date.now();
        timetype = "Upload date";
    }
    obj = {                     // assemble data object
        file: req.file.filename,
        status: "unapproved",
        camera_make: camera  || "No camera data",
        timestamp: timestamp,
        lat: exifdata.latitude || null,
        long: exifdata.longitude || null,
        locationtype: "camera",
        timetype: timetype
    }

    if (obj.lat == null && obj.long == null){ // if no location from exif
        if (req.body.lat != null && req.body.long != null){ // if there is location data from device
            obj.lat = parseFloat(req.body.lat);
            obj.long = parseFloat(req.body.long);
            obj.locationtype = "browser";
        }
        else{                       // if there is no location data at all.
            obj.locationtype = "no location";
            obj.lat = "No location data";
            obj.long = "Not given";
        }   
    }
    if (filename){ // if file upload went correctly, set DB object and return
        await db.set("obs." + filename, obj);
        res.send(filename + " uploaded!");
        console.log(filename + " uploaded!");
    }
    else{           // else dont set DB object
        res.send("failed to upload...");
        console.log("upload failed");
    }
});
e_app.get('/api/data', async function(req,res){ // fetch all image data
    console.log("data fetch request");
    let data = await db.get("obs");
    res.send(data);
});
e_app.get('/api/obs', async function(req,res){ // fetch single image data
    console.log("data fetch request", req.query.key);
    let data = await db.get("obs." + req.query.key);
    res.send({
        key: req.query.key,
        data: data
    });
});

e_app.get('/api/edit', async function(req,res){ // fetch single image data
    if (req.isAuthenticated()) { // require moderator login
        req.query.key, req.query.status
        let obs = await db.get("obs." + req.query.key);
        obs.status = req.query.status;
        await db.set("obs." + req.query.key, obs);
        res.send(obs);
    }
});

e_app.get('/api/firstunapproved', async function(req,res){ // fetch single observation
    console.log('fetch first unapproved');
    let data = await db.get("obs");
    for (const [key, value] of Object.entries(data)) { // key is image name minus extension, value is data obj
        if (data[key].status == "unapproved"){
            res.send({
                key: key,
                data: data[key]
            });
            return 0;
        }
    }
    res.send(null);
    return 1;
});
e_app.get('/api/modupdate', async function(req,res){
    if (req.isAuthenticated()) {
        console.log(req);
    }
});

// =========== App Startup =========== //
e_app.use(express.static(__dirname + '/public'));
e_app.use(express.static(__dirname + '/fileserve'));
const httpsOptions = {
    cert: fs.readFileSync(process.env.CERT),
    ca: fs.readFileSync(process.env.CABUNDLE),
    key: fs.readFileSync(process.env.KEY)
}
const httpsServer = https.createServer(httpsOptions, e_app).listen(process.env.PORT, process.env.HOSTNAME, function (req,res) {
        console.log("HTTPS SERVER STARTED ON " + process.env.HOSTNAME + ":" + process.env.PORT);
});
